//! Parses the mount options in `/etc/mtab` for a list of mount points to ensure
//! that some option is set for them.
//!
//! See the [README.md](https://gitlab.com/SlaynAndKorpil/mount-test/-/blob/master/README.md) on [gitlab](https://gitlab.com/SlaynAndKorpil/mount-test)

use std::fs::File;
use std::io::Read;

/// File to take information about the mount points from.
///
/// The file format is expected to be:
/// ```text
/// <device name> <mount point> <file system> <mount_option1,mount_option2,...> [more options]...
/// ...
/// ```
pub const MOUNT_INFO: &str = "/etc/mtab";

pub const HELP: &str = r#"mount-test

USAGE
mount-test [option]...

where option is:
--help   | -h                   Print this help text.
--option | -o [option]          Specify a mounting option to search for.
--mount  | -m [mount point]...  A list of mount points to search for.
"#;

/// The return code.
enum ReturnCode {
	/// All mount points have the option.
	OptionFound = 0,
	/// Some of the mount points are not mounted using the option.
	OptionNotFound = 1,
	/// Unknown parameter in program args.
	ConfigError = 2,
}

impl From<ReturnCode> for i32 {
	fn from(code: ReturnCode) -> i32 {
		match code {
			ReturnCode::OptionFound => 0,
			ReturnCode::OptionNotFound => 1,
			ReturnCode::ConfigError => 2,
		}
	}
}

struct Config {
	/// The option to search for if any.
	searched_option: Option<String>,
	/// The mount points to check.
	mount_points: Vec<String>,
}

type ParseResult = Result<Config, ParseFail>;

enum ParseFail {
	Help,
	UnknownParameter(String),
}

/// Parse the program args into a [`Config`].
fn parse_args() -> std::io::Result<ParseResult> {
	let mut searched_option = None;
	let mut mount_points = vec![];

	let mut get_search_option = false;
	let mut get_mount_points = false;

	for arg in std::env::args().skip(1) {
		match arg.as_ref() {
			"--option" | "-o" => {
				get_search_option = true;
				get_mount_points = false;
			}
			"--mount" | "-m" => {
				get_search_option = false;
				get_mount_points = true;
			}
			"--help" | "-h" => {
				return Ok(Err(ParseFail::Help));
			}
			_ => {
				if get_search_option {
					searched_option = Some(arg);
				} else if get_mount_points {
					mount_points.push(arg);
				} else {
					return Ok(Err(ParseFail::UnknownParameter(arg)));
				}
			}
		}
	}

	Ok(Ok(Config {
		searched_option,
		mount_points,
	}))
}

fn main() -> std::io::Result<()> {
	let mut mounts = File::open(MOUNT_INFO)?;

	let mut str_buf = String::new();
	mounts.read_to_string(&mut str_buf)?;

	let return_code = match parse_args()? {
		Ok(Config {
			searched_option,
			mount_points,
		}) => {
			let mut message = String::new();

			'line_loop: for line in str_buf.split('\n') {
				let mut parts = line.split(' ');

				if let Some(mount_point) = parts
					.nth(1)
					.filter(|path| mount_points.contains(&String::from(*path)))
				{
					// found one of the mount points

					if let Some(ref searched_option) = searched_option {
						if let Some(mount_options) = parts.nth(1) {
							let mut found_option = false;

							for option in mount_options.split(',') {
								if option == searched_option {
									found_option = true;
								}
							}

							if !found_option {
								message.push_str(&format!("{}\n", mount_point));
							}
						} else {
							eprint!(
								"Could not find mount options for {} in {}.\n\n",
								mount_point, MOUNT_INFO
							);
						}
					} else {
						// no search_option given, just print the entry
						println!("{}", line);
						continue 'line_loop;
					}
				}
			}

			if message.is_empty() {
				ReturnCode::OptionFound
			} else {
				println!("Option not set for mount points:");
				print!("{}", message);
				ReturnCode::OptionNotFound
			}
		}
		Err(error) => match error {
			ParseFail::Help => {
				println!("{}", HELP);

				// nothing found but we want 0 return
				ReturnCode::OptionFound
			}
			ParseFail::UnknownParameter(param) => {
				eprintln!("{}", param);
				ReturnCode::ConfigError
			}
		},
	};

	std::process::exit(return_code.into());
}
