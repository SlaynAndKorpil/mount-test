# mount-test

## Description

Searches `/etc/mtab` for a list of mount points and returns an error code when an option is not set for any of these mount points.

When no option to search for is defined, just prints all matching mount points.

## Usage

`mount-test [option]...`

where option is:

| argument | effect |
| :------- | :----: |
| `--help` or `-h` | Print the help text. |
| `--option` or `-o` [option] | Specify a mounting option to search for. | 
| `--mount` or `-m` [mount point]... | A list of mount points to search for. |

## Return codes

Returns `0` when all of the mount points are mounted with the searched option,
 `1` when some don't have this option or `2` when the program arguments are wrong.

## Install

You first have to install [Rust](https://rustup.rs/) to compile the program.

Then you can:
```terminal
git clone https://gitlab.com/SlaynAndKorpil/mount-test
cd mount-test
cargo build --release
```

The resulting binary is then located in `target/release/mount-test`.

For opening the documentation:
```terminal
cargo doc --open
```

## Example

Example `/etc/mtab` (often symlink to `/proc/mounts`):
```text
sysfs /sys sysfs rw,nosuid,nodev,noexec,relatime 0 0
proc /proc proc rw,nosuid,nodev,noexec,relatime 0 0
udev /dev devtmpfs rw,nosuid,relatime,size=8128448k,nr_inodes=2032112,mode=755 0 0
devpts /dev/pts devpts rw,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=000 0 0
tmpfs /run tmpfs rw,nosuid,nodev,noexec,relatime,size=1639236k,mode=755 0 0
/dev/sdc6 /home ext4 rw,relatime 0 0
```

You can then test if any of the mount points `/sys`, `/proc` or `/home` are mounted with the `ro` option:
```terminal
felix@test:~/mount-test$ ./target/release/mount-test --mount /sys /proc /home --option ro
Option not set for mount points:
/sys
/proc
/home
felix@test:~/mount-test$ echo $?
1
```

...or if they are mounted as `nodev`:
```terminal
felix@test:~/mount-test$ ./target/release/mount-test -o nodev -m /sys /proc /home
Option not set for mount points:
/home
felix@test:~/mount-test$ echo $?
1
```

...or if they are mounted as `rw`:
```terminal
felix@test:~/mount-test$ ./target/release/mount-test -o nodev -m /sys /proc /home
felix@test:~/mount-test$ echo $?
0
```

...or without searching for an option:
```terminal
felix@test:~/mount-test$ ./target/release/mount-test --mount /sys /proc /home
sysfs /sys sysfs rw,nosuid,nodev,noexec,relatime 0 0
proc /proc proc rw,nosuid,nodev,noexec,relatime 0 0
/dev/sdc6 /home ext4 rw,relatime 0 0
felix@test:~/mount-test$ echo $?
0
```
